import os
#Modulos===========================================
def menu(o):
    print("================Menu================")
    print("1- Registrar un producto")
    print("2- Mostrar productos registrados")
    print("3- Ver productos desde, hasta")
    print("4- Agregar cantidad al stock menor a...")
    print("5- Eliminar productos sin stock")
    print("\n6- Salir")
    print("====================================")
    o = int(input("\nElija una opcion: "))

    return o

def codigoProducto():
    c = input("Ingrese el codigo del producto: ")
    while (c==''):
        c = input("Error: Espacio en blanco\nIntente de nuevo: ")

    return c

def descripcionProducto():
    d = input("Agregue su descripcion: ")
    while (d==''):
        d = input("Error: Espacio en blanco\nIntente de nuevo: ")
    
    return d

def precioProducto():
    pasa = False
    
    while (pasa==False):
        p = float(input("Ingrese valor de producto: "))
        if(p<0):
            print("El valor no puede ser negativo")
        else:
            break
    return p

def stockProducto():
    pasa = False
    
    while (pasa==False):
        s = int(input("Ingrese el stock: "))
        if(s<0):
            print("El stock no puede ser negativo")
        else:
            return s
    


def registro(productos):
    print("======Registro de productos======\n(todos los valores son obligatorios)")
    codigo = int(codigoProducto())
    if(codigo not in productos):
        descripcion = descripcionProducto()
        precio = float(precioProducto())
        stock = int(stockProducto())
        productos[codigo] = [descripcion, precio, stock]

        print("------------------------------------")
        print("Su producto se registro con exito.")
        print("------------------------------------")
    else:
        print("--------------------------------")
        print("Este producto ya fue registrado.")
        print("--------------------------------")


    
    input("Presione una tecla para volver al menu....")

def mostrar(productos):
    print("====Lista de productos====")
    print("------------------------------------")
    if(len(productos)>=1):
        for codigo, datos in productos.items():
            print(codigo, datos)
    else:
        print("Aun no se registro ningun producto.")    
    print("------------------------------------")
    input("Presione una tecla para volver al menu....")

def verDesdeHasta(productos):
    print("====Lista de Producto de un intervalo especificado====")
    print("--------------------------------------------------------")
    if(len(productos)>=1):
        desde = int(input("Ingrese valor inicial: "))
        hasta = int(input("Ingrese valor final: "))
        if(len(productos)>=1):
            for codigo, datos in productos.items():
                if(datos[2]>=desde) and (datos[2]<=hasta):
                    print(codigo, datos)
    else:
        print("La lista esta vacia, registre productos con la opcion 1.")
    print("--------------------------------------------------------")
    input("Presione una tecla para volver al menu....")

def sumarStock(productos):
    print("======Agregar stock======")
    print("------------------------------------")
    if(len(productos)>=1):   
        stock = int(input("Ingrese un valor ha agregar: "))
        menor = int(input("Agregar stock a los productos en stock menores que: "))
        for codigo, datos in productos.items():
                if(datos[2]<menor):
                    datos[2]+= stock
                    print(codigo, datos)
                    print("Se agrego correctamente")
    else:
        print("No hay ningun producto registrado.")
    print("------------------------------------")
    input("Presione una tecla para volver al menu....")

def eliminar(productos):
    print("======Eliminar productos sin stock======")
    print("------------------------------------")
    if(len(productos)>=1):
        i = len(productos)
        aux = productos.copy()
        for codigo, datos in productos.items():
            if(datos[2]==0):
                print("Se elimino")
                print(datos[0], "con codigo", codigo)
                aux.pop(codigo)

        productos = aux.copy()
        if(len(productos)==i):
            print("No se elimino ningun producto.")
    else:
        print("No hay ningun producto registrado.")
    print("------------------------------------")
    input("Presione una tecla para volver al menu....")

    return productos

#Principal=========================================

productos = {}

#Lista precargada
#productos = {21421:['Gel para cabello', 75.5, 20], 11251:['Shampoo', 150, 26], 135123:['PC Armada', 255.50, 2], 1325123:['Hamburguesa', 255.50, 76], 1300123:['mp2', 255.50, 0]}

opc = 0
while(opc!=6):
    os.system('cls')
    opc=menu(opc)

    if(opc==1):
        os.system('cls')
        registro(productos)

    if(opc==2):
        os.system('cls')
        mostrar(productos)

    if(opc==3):
        os.system('cls')
        verDesdeHasta(productos)

    if(opc==4):
        os.system('cls')
        sumarStock(productos)

    if(opc==5):
        os.system('cls')
        productos = eliminar(productos)
    
    if(opc==6):
        print("-------------------")
        input("Fin del programa.")